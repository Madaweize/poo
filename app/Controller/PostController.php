<?php


namespace App\Controller;


class PostController extends AppController
{

    public function __construct()
    {
        parent::__construct();
        $this->loadModel('Post');
    }

    public function index(){
      $posts=$this->Post->all();
      $this->render('posts/index',compact('posts'));

    }



    public function categories(){

    }

    public function show(){

    }

}