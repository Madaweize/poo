<?php


use Core\Config;

class App
{
    protected static $_instance;
    protected $db_instance;


    public static function  getInstance(){
        if(self::$_instance==null){
            self::$_instance=new self();
        }

        return self::$_instance;
    }

    public function getTable($class_name){
         $class_name='App\Model\\'.ucfirst($class_name).'Model';
         return new $class_name($this->getDb());
    }


    public function getDb(){
        if($this->db_instance==null){
            $conf=Config::getInstance(ROOT.'/config/config.php');
            $this->db_instance=new \Core\Database\MysqlDataBase($conf->getValue('db_host'),$conf->getValue('db_name'),$conf->getValue('db_user'),$conf->getValue('db_pass'));

        }

        return $this->db_instance;
    }

    public static function load(){

        require ROOT.'/app/Autoloader.php';
        \App\Autoloader::register();

        require ROOT.'/core/Autoloader.php';
        \Core\Autoloader::register();
    }

}