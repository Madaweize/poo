<?php


namespace Core\Controller;


class Controller
{

    protected $template;
    protected $viewPath;


    public function render($view,$variables=[]){
        ob_start();
        extract($variables);
        require ROOT.$this->viewPath.'/'.$view.'.php';
        $content=ob_get_clean();
        require ROOT.$this->viewPath.'/templates/'.$this->template.'.php';
    }

}