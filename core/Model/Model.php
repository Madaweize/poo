<?php


namespace Core\Model;


use Core\Database\Database;

class Model
{
    protected $db;
    protected $table;

    public function __construct(Database $db)
    {
        $this->db=$db;
    }


    public function query($statement,$attributes=null,$one=false){
        if($attributes){
            $data=$this->db->prepare($statement,$attributes,str_replace('Model','Entity',get_class($this)),$one);
        }else{
            $data=$this->db->query($statement,str_replace('Model','Entity',get_class($this)),$one);
        }

        return $data;
    }


    public function all(){
        return $this->query('SELECT * FROM '.$this->table);
    }

}