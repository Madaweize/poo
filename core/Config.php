<?php


namespace Core;


class Config
{

    protected static $_instance;
    protected $settings;

    public function __construct($file)
    {
        $this->settings=require ($file);
    }


    public static function getInstance($file){
        if(self::$_instance===null){
            self::$_instance=new self($file);
        }
        return self::$_instance;
    }


    public function getValue($key){
        if(array_key_exists($key,$this->settings)){
            return $this->settings[$key];
        }

        return null;
    }
}