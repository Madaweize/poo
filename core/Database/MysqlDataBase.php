<?php


namespace Core\Database;


use Core\Config;

use PDO;
use PDOException;

class MysqlDataBase extends Database
{


    protected $db_host;
    protected $db_name;
    protected $db_user;
    protected $db_pass;
    protected $pdo;

    public function __construct($db_host='localhost',$db_name='blog',$db_user='root',$db_pass='')
    {
        $this->db_host=$db_host;
        $this->db_name=$db_name;
        $this->db_user=$db_user;
        $this->db_pass=$db_pass;
    }

    public function getPDO(){
        $conn = NULL;
        if($this->pdo==null){

            try{
                $conn = new PDO("mysql:host={$this->db_host};dbname={$this->db_name}", $this->db_user,$this->db_pass);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e){
                echo 'ERROR: ' . $e->getMessage();
            }

            $this->pdo = $conn;
        }

        return $this->pdo;


    }


    public function prepare($statement,$attributes,$class_name=null,$one=false){
        $pdoStatement=$this->getPDO()->prepare($statement);
        $pdoStatement->execute($attributes);

        if (
            strpos($statement,'UPDATE')===0 ||
            strpos($statement,'INSERT')===0 ||
            strpos($statement,'DELETE')===0
        ){
            return null;
        }

        if ($class_name==null){
            $pdoStatement->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $pdoStatement->setFetchMode(PDO::FETCH_CLASS,$class_name);
        }

        if($one){
            $data=$pdoStatement->fetch();
        }else{
            $data=$pdoStatement->fetchAll();
        }

        return $data;
    }


    public function query($statement,$class_name=null,$one=false){


        $pdoStatement=$this->getPDO()->query($statement);

        if (
            strpos($statement,'UPDATE')===0 ||
            strpos($statement,'INSERT')===0 ||
            strpos($statement,'DELETE')===0
        ){
            return null;
        }

        if ($class_name==null){
            $pdoStatement->setFetchMode(PDO::FETCH_OBJ);
        }else{
            $pdoStatement->setFetchMode(PDO::FETCH_CLASS,$class_name);
        }

        if($one){
            $data=$pdoStatement->fetch();
        }else{
            $data=$pdoStatement->fetchAll();
        }

        return $data;
    }

    public function lastInsertId(){
        return $this->getPDO()->lastInsertId();
    }


}