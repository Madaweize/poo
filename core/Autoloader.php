<?php

namespace Core;

class Autoloader
{

    public static function autoload($class)
    {

        $arrayClass = explode('\\', $class);
        if (__NAMESPACE__ === $arrayClass[0]) {

            if (strpos($class, __NAMESPACE__ . '\\') === 0) {
                $class = str_replace(__NAMESPACE__ . '\\', '', $class);
                $class = str_replace('\\', '/', $class);
            }

            require ROOT . '/core/' . $class . '.php';
        }
    }

    public static function register(){
        spl_autoload_register(array(__CLASS__,'autoload'));
    }
}


