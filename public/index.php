<?php

define('ROOT',dirname(__DIR__));
require ROOT.'/app/App.php';

App::load();

if(isset($_GET['page'])){
    $page=$_GET['page'];
}else{
    $page='post.index';
}


if ($page==='post.index'){
    $controller=new \App\Controller\PostController();
    $controller->index();
}elseif ($page='post.show'){
    $controller=new \App\Controller\PostController();
    $controller->show();
}elseif ($page='post.categories'){
    $controller=new \App\Controller\PostController();
    $controller->categories();
}